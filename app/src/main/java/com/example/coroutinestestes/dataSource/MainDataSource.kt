package com.example.coroutinestestes.dataSource

interface MainDataSource {
    suspend fun fetchAndroidCodeNames(): List<String>
}