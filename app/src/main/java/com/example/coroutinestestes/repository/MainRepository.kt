package com.example.coroutinestestes.repository

import com.example.coroutinestestes.dataSource.MainDataSource

class MainRepository(private val mainDataSource: MainDataSource) {

    suspend fun fetchAndroidCodeNames() = mainDataSource.fetchAndroidCodeNames()
}