package com.example.coroutinestestes.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Adapter
import android.widget.Toast
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.coroutinestestes.dataSource.MainDataSourceImpl
import com.example.coroutinestestes.databinding.ActivityMainBinding
import com.example.coroutinestestes.repository.MainRepository
import com.example.coroutinestestes.ui.MainViewModel.MainViewModelFactory
import com.example.coroutinestestes.ui.adapter.MainAdapter
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var recyclerView: RecyclerView
    private lateinit var viewManager: RecyclerView.LayoutManager
    private lateinit var viewAdapter: RecyclerView.Adapter<*>

    private val viewModel by viewModels<MainViewModel> {
        val mainDataSource = MainDataSourceImpl()
        val mainRepository = MainRepository(mainDataSource)
        MainViewModelFactory(mainRepository = mainRepository)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //initializeDependencies()

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        viewManager = LinearLayoutManager(this)
//        viewAdapter = MainAdapter(myDataSet) TODO: Set DataSet to adapter

        bindView()

        viewModel.uiState().observe(this, Observer { uiState ->
            if (uiState != null) {
                render(uiState)
            }
        })
    }

    private fun bindView() {
      recyclerView = recycle_versions.apply {
          setHasFixedSize(true)
          layoutManager = viewManager
          adapter = viewAdapter
      }

        btn_click_me.setOnClickListener {
            viewModel.getAndroidCodeNames()
        }
    }

    private fun render(uiState: UiState) {
        when (uiState) {
            is UiState.Loading -> {
                onLoad()
            }
            is UiState.Success -> {
                onSuccess(uiState)
            }
            is UiState.Error -> {
                onError(uiState)
            }
        }
    }

    private fun onLoad() = with(binding) {
        progressBar.visibility = View.VISIBLE
        btnClickMe.isEnabled = false
    }

    private fun onSuccess(uiState: UiState.Success) = with(binding) {
        progressBar.visibility = View.GONE
        btnClickMe.isEnabled = true
        // txt_list.text = "${txt_list.text}/n${uiState.recentVersions}" TODO: Changes to recycleView
    }

    private fun onError(uiState: UiState.Error) = with(binding) {
        progressBar.visibility = View.GONE
        btnClickMe.isEnabled = true
        Toast.makeText(this@MainActivity, uiState.message, Toast.LENGTH_LONG).show()
    }

//    private fun initializeDependencies() {
//        val mainDataSource = MainDataSource()
//        val mainRepository = MainRepository(mainDataSource)
//
//        viewModel = ViewModelProvider(
//            this,
//            MainViewModelFactory(mainRepository)
//        ).get(MainViewModel::class.java)
//    }
}


