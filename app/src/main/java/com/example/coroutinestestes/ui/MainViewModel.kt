package com.example.coroutinestestes.ui

import androidx.lifecycle.*
import com.example.coroutinestestes.repository.MainRepository
import kotlinx.coroutines.launch
import java.lang.Exception

class MainViewModel(private val mainRepository: MainRepository) : BaseViewModel<UiState>() {

    fun getAndroidCodeNames() {
        uiState.value = UiState.Loading
        viewModelScope.launch {
            try {
                val nameRepetition = mainRepository.fetchAndroidCodeNames()
                uiState.value = UiState.Success(nameRepetition)
            } catch (exception: Exception) {
                uiState.value = UiState.Error("Network Request failed!")
            }
        }
    }

    class MainViewModelFactory(private val mainRepository: MainRepository) :
        ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return modelClass.getConstructor(MainRepository::class.java).newInstance(mainRepository)
        }
    }
}