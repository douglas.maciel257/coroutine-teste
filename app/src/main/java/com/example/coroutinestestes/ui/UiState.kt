package com.example.coroutinestestes.ui

sealed class UiState {
    object Loading : UiState()
    data class Success(val recentVersions: List<String>) : UiState()
    data class Error(val message: String) : UiState()
}