package com.example.coroutinestestes

import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Rule
import org.junit.rules.TestRule
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.coroutinestestes.fake.FakeErrorMainDataSource
import com.example.coroutinestestes.fake.FakeSuccessMainDataSource
import com.example.coroutinestestes.mock.MockAndroidCodenames
import com.example.coroutinestestes.repository.MainRepository
import com.example.coroutinestestes.ui.MainViewModel
import com.example.coroutinestestes.ui.UiState
import com.example.coroutinestestes.utils.CoroutineTestRule
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.*
import org.junit.Test


@ExperimentalCoroutinesApi
class MainViewModelTest {

    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    @get: Rule
    val coroutineTestRule: CoroutineTestRule = CoroutineTestRule()

    private val receivedUiStates: MutableList<UiState> =
        arrayListOf()

    @Test
    fun `should return Success when network request is successful`() =
        coroutineTestRule.runBlockingTest {
            val mainDataSource = FakeSuccessMainDataSource()
            val mainRepository = MainRepository(mainDataSource)
            val mainViewModel = MainViewModel(mainRepository)

            observeViewModel(mainViewModel)

            assertTrue(receivedUiStates.isEmpty())

            mainViewModel.getAndroidCodeNames()
            advanceTimeBy(5000)

            assertEquals(
                listOf(
                    UiState.Loading,
                    UiState.Success(mockRecentVersions())
                ),
                receivedUiStates
            )
        }

    @Test
    fun `should return Error when network fails`() =
        coroutineTestRule.runBlockingTest {
            val fakeMainDataSource = FakeErrorMainDataSource()
            val mainRepository = MainRepository(fakeMainDataSource)
            val mainViewModel = MainViewModel(mainRepository)

            observeViewModel(mainViewModel)

            assertTrue(receivedUiStates.isEmpty())

            mainViewModel.getAndroidCodeNames()
            advanceTimeBy(5000)

            assertEquals(
                listOf(
                    UiState.Loading,
                    UiState.Error(mockErrorMessage())
                ),
                receivedUiStates
            )
        }

    private fun mockRecentVersions() = MockAndroidCodenames.listNames

    private fun mockErrorMessage() = "Network Request failed!"

    private fun observeViewModel(viewModel: MainViewModel) {
        viewModel.uiState().observeForever { uiState ->
            if (uiState != null) {
                receivedUiStates.add(uiState)
            }
        }
    }
}