package com.example.coroutinestestes.fake

import com.example.coroutinestestes.dataSource.MainDataSource
import kotlinx.coroutines.delay
import java.io.IOException

class FakeErrorMainDataSource : MainDataSource {
    @Throws(IOException::class)
    override suspend fun fetchAndroidCodeNames(): List<String> {
        delay(5000)
        throw IOException("No internet connection")
    }
}