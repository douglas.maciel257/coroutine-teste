package com.example.coroutinestestes.mock

object MockAndroidCodenames {
    val listNames = listOf(
        "No codename\t1.0 - 1.1",
        "Cupcake\t1.5",
        "Donut\t1.6",
        "Eclair\t2.0 – 2.1",
        "Froyo\t2.2 – 2.2.3",
        "Gingerbread\t2.3 – 2.3.7",
        "Honeycomb\t3.0 – 3.2.6",
        "Ice Cream Sandwich\t4.0 – 4.0.4",
        "Jelly Bean\t4.1 – 4.3.1",
        "KitKat\t4.4 – 4.4.4",
        "Lollipop\t5.0 – 5.1.1",
        "Marshmallow\t6.0 – 6.0.1",
        "Nougat\t7.0 – 7.1.2",
        "Oreo\t8.0 – 8.1",
        "Pie\t9.0",
        "Android 10\t10.0",
        "Android 11\t11.0"
    )
}